# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_11_02_02.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


from card import Card
from card_list import CardList
import options


class CardTable:
    """Класс CardTable представляет игорный стол.

    Умеет:
      - хранить карты, лежащие на столе;
      - "отдавать" карту;
      - определять - пуст или нет.

    Атрибуты экземпляра класса:
      - self._card_list (CardList): набор карт на столе.

    Методы экземпляра класса:
      - self.take_card(): берет карту со стола;
      - self.is_empty(): True, если на слоле нет карт.

    Свойства:
      - card_count (int): количество карт на столе.
    """

    def __init__(self, cards_count):
        """Инициализация стола.

        Параметры:
          - cards_count (int): количество карт для игры.

        При инициализации стола происходит генерация набора
        из 'cards_count' карт (номерами от 1 до 'cards_count').
        Если options.debug == True, карты должны быть лицом вверх.
        После генерации их необходимо перемешать -
        используйте self._card_list.shuffle().

        Необходимо удостовериться, что 'cards_count' > 1.
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def __str__(self):
        """Вернуть строковое представление карт на столе.

        Формат:

        Карты на столе (3): X X X
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def take_card(self, index):
        """Взять (вернуть) со стола одну карту под номером 'index'.

        Параметры:
          - 'index' - номер карты, начиная с 1.

        Исключения:
          - IndexError: если не 1 <= index <= card_count.
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def is_empty(self):
        """Вернуть True, если на столе нет карт."""
        raise NotImplementedError
        # Уберите raise и допишите код

    @property
    def card_count(self):
        """Вернуть количество карт на столе."""
        raise NotImplementedError
        # Уберите raise и допишите код
