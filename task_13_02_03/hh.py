# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_13_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import requests
import exceptions


class Hh:
    """Класс Parser реализует парсер вакансий с сайта hh.ru."""

    @staticmethod
    def _get_area_id(area):
        """Получить ID региона по его имени.

        Параметры:
          - area (str): регион (например, Москва);

        Результат:
          - ID региона;
          или
          - None, если пришла ошибка в ответе на запрос или регион не найден.

        Переданные параметры необходимо преобразовать в параметры запроса:
          - API-метод:
            https://api.hh.ru/areas;
          - документация:
            https://github.com/hhru/api/blob/master/docs/areas.md.
        """

        def get_area(areas, name):
            """Вернуть ID города 'name' из структуры 'areas'."""
            for item in areas:
                if "name" in item and item["name"] == name:
                    return int(item["id"])
                if "areas" in item and item["areas"]:
                    res = get_area(item["areas"], name)
                    if res:
                        return res

        raise NotImplementedError
        # Уберите raise и дополните код

    @staticmethod
    def _get_experience_id(experience):
        """Получить тип опыта (строку) в зависимости от 'experience' лет.

        На hh.ru опыт делится на 4 категории:
         - "noExperience": нет опыта;
         - "between1And3": от 1 до 3 лет (не включительно);
         - "between3And6": от 3 до 6 лет (не включительно);
         - "moreThan6": больше 6 лет.

        Результат:
          - одна из категорий выше в зависимости от 'experience' лет;
          или
          - ValueError, если 'experience' не является
            целым положительным числом.
        """
        raise NotImplementedError
        # Уберите raise и дополните код

    @staticmethod
    def search(title, area, salary=None, experience=None):
        """Выполнить поиск и вернуть список вакансий.

        Параметры:
          - title (str): наименование вакансии;
          - area (str): регион;
          - salary (int или float): уровень зарплаты;
          - experience (int): количество лет опыта.

        Переданные параметры необходимо преобразовать в параметры запроса:
          - основной API-метод:
            https://api.hh.ru/vacancies;
          - документация:
            https://github.com/hhru/api/blob/master/docs/vacancies.md.

        Поиск включает ряд шагов:
        1. Найти вакансии и получить ID вакансий.
        2. Для каждой найденной ссылки получить информацию по ID.

        Результат:
          - список словарей вакансий.
        """

        # Только первые 10 результатов
        params = dict(text=title, search_field="name", per_page=10)

        raise NotImplementedError
        # Уберите raise и дополните код

        # Запрос для каждого элемента из vacancies["items"] по 'id'
        res = []

        raise NotImplementedError
        # Уберите raise и дополните код

        return res
